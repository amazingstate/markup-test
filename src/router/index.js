import Vue from 'vue'
import VueRouter from 'vue-router'
import Letter from '../views/Letter.vue'

Vue.use(VueRouter)

const routes = [{
  path: '/',
  name: 'Letter',
  component: Letter
}]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router