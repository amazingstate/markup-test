# test-ui



## Note
```
Верстал 14 макет - пк версию, ушло полтора часа, к сожалению, не успел доделать футер и в идеале адаптацию сделать, но чтобы не затягивать выкладываю, я думаю уже понятно будет стоит ли делать дальше и работать в самом проекте.
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
